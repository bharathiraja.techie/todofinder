﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TODOFinder
{
    public class Finder
    {
        private string _path;
        public Finder(string path)
        {
            _path = path;
        }
        public string[] getFiles()
        {
            return Directory.GetFiles(_path, "*", SearchOption.AllDirectories);
        }
        public string[] FindFiles(string[] files)
        {
            List<string> foundFiles = new List<string>();
            if (files.Length > 0)
            {
                foreach (var item in files)
                {
                    if (File.Exists(item))
                        if (File.ReadAllText(item).Contains("TODO"))
                            foundFiles.Add(item);

                }
            }
            return foundFiles.ToArray();
        }
        public void Print(string[] files)
        {
            foreach (var item in files)
            {
                Console.WriteLine(item);
            }
        }
    }
}
