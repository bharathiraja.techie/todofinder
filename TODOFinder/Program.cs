﻿using System;
using System.IO;

namespace TODOFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Directory.GetCurrentDirectory();
            Finder finder = new Finder(path);
            string[] files = finder.getFiles();
            string[] foundFiles = finder.FindFiles(files);
            if (foundFiles.Length > 0)
            {
                foreach (var item in foundFiles)
                {
                    Console.WriteLine(item);
                }
            }
         }

       
    }
}
