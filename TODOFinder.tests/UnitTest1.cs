using System;
using System.IO;
using Xunit;

namespace TODOFinder.tests
{
    public class UnitTest1
    {
        private readonly Finder _finder;

        public UnitTest1()
        {
            string path = Directory.GetCurrentDirectory();
            _finder = new Finder(path);
        }
        [Fact]
        public void DirectoryHaveFiles()
        {
            string[] files = _finder.getFiles();

            Assert.NotNull(files);
        }

        [Fact]
        public void ShouldReturnEmptyArrayIfNotFound()
        {
            string[] files = { };

            string[] foundFiles = _finder.FindFiles(files);

            Assert.Equal(foundFiles.Length, 0);
        }


    }
}
